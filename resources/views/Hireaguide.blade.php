<a class="sf-button standard accent standard" href="#modal-1" role="button" data-toggle="modal"><span class="text">Hire a guide</span></a>
<div id="modal-1" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="Modal Example" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="sf-icon-remove"></i></button>
                <h3 id="modal-label"></h3></div>
            <div class="modal-body">
                <div class="inner-container-wrap">
                    <div class="container">
                        <div class="content-divider-wrap">
                            <div class="content-divider sf-elem-bb"></div>
                        </div>
                        <div class="inner-page-wrap clearfix">
                            <div class="clearfix">
                                <div class="page-content clearfix">
                                    <div class="clearfix" id="12589">
                                        <div class="woocommerce">

                                            <form class="form-horizontal form-simple" method="POST" action="{{ url('Hireaguide') }}" enctype="multipart/form-data">
                                                {{ csrf_field() }}

                                                <div class="container"></div>
                                                <div class="col-sm-9 col-md-offset-2" id="customer_details">
                                                    <div>
                                                        <div class="woocommerce-billing-fields">
                                                            <div class="clear"></div>
                                                            <p class="form-row form-row form-row-last validate-required" id="billing_last_name_field">
                                                                <label for="billing_last_name" class="">attractionid</label>
                                                                <input type="text" class="input-text" name="attractionid" id="billing_last_name" value="{{$data->id}}"/>
                                                            </p>
                                                            <p class="form-row form-row form-row-last validate-required" id="billing_last_name_field">
                                                                <label for="billing_last_name" class="">Name</label>
                                                                <input type="text" class="input-text" name="name" id="billing_last_name" placeholder="Enter your name"  required/>
                                                            </p>

                                                            <p class="form-row form-row form-row-last validate-required" id="billing_last_name_field">
                                                                <label for="billing_last_name" class="">Email</label>
                                                                <input type="email" class="input-text" name="email"  id="billing_last_name" placeholder="Enter your email"  required/>
                                                            </p>

                                                            <p class="form-row form-row form-row-last validate-required" id="billing_last_name_field">
                                                                <label for="billing_last_name" class="">Contact Number</label>

                                                                <input type="number" class="input-text" name="contactnumber" id="billing_last_name" placeholder="Enter your Number" required/>
                                                            </p>

                                                            <p class="form-row form-row form-row-last validate-required" id="billing_last_name_field">
                                                                <label for="billing_last_name" class="">Country of Residence</label>
                                                                <select class="country_to_state country_select billing_country" name="country"  required>
                                                                    <option value="{{ old('typeofmembership') }}">{{ old('typeofmembership') }}</option>
                                                                    @foreach($listcountries as $data)
                                                                        <option value="{{$data->nicename}}">{{$data->nicename}}</option>
                                                                    @endforeach

                                                                </select>
                                                                {{--<input type="text" class="input-text" name="billing_last_name" id="billing_last_name" placeholder="Enter your Country of Residence" value="country" />--}}
                                                            </p>


                                                            <p class="form-row form-row form-row-last validate-required" id="billing_last_name_field">
                                                                <label for="billing_last_name" class="">Total Adults</label>
                                                                <input type="number" class="input-text" name="totaldults" id="billing_last_name" placeholder="Total Adults"  required/>
                                                            </p>



                                                            <p class="form-row form-row form-row-last validate-required" id="billing_last_name_field">
                                                                <label for="billing_last_name" class="">Total Children</label>
                                                                <input type="number" class="input-text" name="totalchildren" id="billing_last_name" placeholder="Total Children"  required/>
                                                            </p>



                                                            <p class="form-row form-row notes" id="order_comments_field"><label for="order_comments" class="">Message</label>
                                                                <textarea class="input-text " id="order_comments" rows="2" cols="5" placeholder="Type your message" name="message" required></textarea>
                                                            </p>
                                                            <p class="form-row form-row notes"><input type="submit" value="Send Message" class="wpcf7-form-control wpcf7-submit"  style="margin: 0px !important;"/></p>
                                                        </div>

                                                    </div>

                                                </div>
                                            </form>
                                        </div>
                                        <div class="link-pages"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div id="sf-full-header-search-backdrop"></div>
                </div>
            </div>
        </div>
    </div>
</div>