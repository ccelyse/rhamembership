@extends('layouts.master')

@section('title', 'RHA')

@section('content')

    @include('layouts.topmenu')
<style>
    .sf-button.accent {
        color: #fff;
        background-color: #6b442b;
        border-color: #6b442b;
    }
</style>
    <div id="sf-mobile-slideout-backdrop"></div>
    @foreach($listmore as $data)
<div id="main-container" class="clearfix">
    <div class="fancy-heading-wrap  fancy-style">
        <div class="page-heading fancy-heading clearfix light-style fancy-image  page-heading-breadcrumbs" style="background-image: url('attractions/{{$data->attraction_image}}');" data-height="475" data-img-width="2000" data-img-height="800">
            <span class="media-overlay" style="background-color:transparent;opacity:0.5;"></span>
            <div class="heading-text container" data-textalign="left">
                <h1 class="entry-title">{{$data->attraction_name}}</h1>
            </div>
        </div>
    </div>

    <div class="inner-container-wrap">
        <div class="inner-page-wrap has-no-sidebar no-bottom-spacing no-top-spacing clearfix">
            <div class="clearfix">
                <div class="page-content hfeed clearfix">
                    <div class="clearfix post-14975 page type-page status-publish hentry" id="14975">
                        <section data-header-style="" class="row fw-row  dynamic-header-change">
                            <div class="spb-row-container spb-row-full-width col-sm-12  col-natural" data-row-style="" data-v-center="true" data-top-style="none" data-bottom-style="none" style="padding-left:1%;padding-right:1%;margin-top:0px;margin-bottom:0px;">
                                <div class="spb_content_element" style="padding-top:0px;padding-bottom:0px;">
                                    <section class="container ">
                                        <div class="row">
                                            <div class="blank_spacer col-sm-12" style="height:30px;"></div>
                                        </div>
                                    </section>
                                    <section class="container ">
                                        <div class="row">
                                            <div class="spb_content_element col-sm-10   col-md-offset-1 spb_text_column">
                                                <div class="spb-asset-content" style="padding-top:0%;padding-bottom:0%;padding-left:0%;padding-right:0%;">

                                                    @if (session('success'))
                                                        <div class="alert alert-success" id="success_messages" style="margin-top: 10px;">
                                                            {{ session('success') }}
                                                        </div>
                                                    @endif

                                                    <?php
                                                    $str = $data->attraction_indetails;
                                                    ?>
                                                    <div><?php echo $str;?></div>
                                                </div>
                                                {{--<a class="sf-button standard accent standard" href="#" role="button"><span class="text">Explore restaurants, bars and nightclubs in {{$data->attraction_province}}</span></a><br>--}}
                                                {{--<a class="sf-button standard accent standard" href="#" role="button"><span class="text">Book a hotel in {{$data->attraction_province}}</span></a>--}}
                                            </div>

                                        </div>
                                    </section>
                                </div>
                            </div>
                        </section>
                        <div class="link-pages"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="sf-full-header-search-backdrop"></div>
</div>
@endforeach
    @include('layouts.footer')
@endsection