<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateJoinMemberTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('JoinMember', function (Blueprint $table) {
            $table->increments('id');
            $table->string('typeofmembership');
            $table->string('chamber');
            $table->string('companyname');
            $table->string('companycode');
            $table->string('hotelcategory');
            $table->string('numberofrooms');
            $table->string('gender');
            $table->string('phonenumber');
            $table->string('pobx');
            $table->string('email');
            $table->string('website');
            $table->string('businessaddress');
            $table->string('buildingname');
            $table->string('businessarea');
            $table->string('province');
            $table->string('district');
            $table->string('sector');
            $table->string('cell');
            $table->string('companytype');
            $table->string('ownership');
            $table->string('businessactivity');
            $table->string('export');
            $table->string('numberofemployees');
            $table->string('numberofemployeesapart');
            $table->string('documentname');
            $table->string('identification')->nullable();
            $table->string('approval')->nullable();
            $table->string('days_left')->nullable();
            $table->string('daysmail30')->nullable();
            $table->string('daysmail15')->nullable();
            $table->string('daysmail6')->nullable();
            $table->string('daysmail0')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('JoinMember');
    }
}
