<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class SendMailDays15 extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'Member:SendMailDays15';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Cron job to send a reminder mail of 15 days membership expiration';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $listmembers = JoinMember::whereBetween('days_left', [7, 15])->get();
        foreach ($listmembers as $members){

            $names = $members->companyname;
            $mail_status = $members->daysmail15;
            if($mail_status == null){
                $update_status = JoinMember::where('id',$members->id)->update(['daysmail15'=>"sent"]);
                $content = [
                    'title'=> $names,
                    'body'=> 'We would like to remind you that your membership will expire in 15 days',
//            'button' => 'Click Here'
                ];
                $receiverAddress = $members->email;

                Mail::to($receiverAddress)->send(new RenewMembership($content));

                dd('mail sent successfully');
            }else{
                echo 'already notified';
            }

        }
        $this->info('Successfully updated mail status');
    }
}
