<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Hireaguide extends Model
{
    protected $table = "Hireaguide";
    protected $fillable = ['id','attractionid','name','email','contactnumber','country','totaldults','totalchildren','message'];
}
