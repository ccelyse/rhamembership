@component('mail::message')
<strong>Hello,{{ $content['title'] }}</strong>

{{ $content['body'] }}

{{--@component('mail::button', ['url' => ''])--}}
{{--Button Text--}}
{{--@endcomponent--}}

Thanks,<br>
<strong>RHA</strong>
@endcomponent
