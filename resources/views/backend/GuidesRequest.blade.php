@extends('backend.layout.master')

@section('title', 'RHA')

@section('content')

    @include('backend.layout.sidemenu')
    @include('backend.layout.upmenu')
    <style>
        .btn-secondary{
            color:#fff !important;
            background-color: #6a442b !important;
            border-color:#6a442b !important;
        }
    </style>
    <div class="app-content content">
        <div class="content-wrapper">

            <div class="content-body">
                <!-- Complex headers with column visibility table -->
                <section id="complex-header">
                    <div class="row">
                        <div class="col-12">
                            <div class="card">
                                <div class="card-header">
                                    <h4 class="card-title">Guides Requests</h4>
                                    @if (session('success'))
                                        <div class="alert alert-success" id="success_messages" style="margin-top: 10px;">
                                            {{ session('success') }}
                                        </div>
                                    @endif
                                    <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
                                    <div class="heading-elements">
                                        <ul class="list-inline mb-0">
                                            <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                                            <li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>
                                            <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                                            <li><a data-action="close"><i class="ft-x"></i></a></li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="card-content collapse show">
                                    <div class="card-body card-dashboard">
                                        <table class="table table-striped table-bordered datacol-cixedcolumns-integration table-responsive">
                                            <thead>

                                            <tr>
                                                <th>Name</th>
                                                <th>Email</th>
                                                <th>Contact Number</th>
                                                <th>Country</th>
                                                <th>Total Adults</th>
                                                <th>Total Children</th>
                                                <th>message</th>
                                                <th>Date Created</th>
                                            </tr>

                                            </thead>
                                            <tbody>
                                            @foreach($listguides as $data)
                                                <tr>
                                                    <td>{{$data->name}}</td>
                                                    <td>{{$data->email}}</td>
                                                    <td>{{$data->contactnumber}}</td>
                                                    <td>{{$data->country}}</td>
                                                    <td>{{$data->totaldults}}</td>
                                                    <td>{{$data->totalchildren}}</td>
                                                    <td>{{$data->message}}</td>
                                                    <td>{{$data->created_at}}</td>
                                                </tr>
                                            @endforeach
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        </div>
    </div>
    <script src="backend/app-assets/vendors/js/vendors.min.js" type="text/javascript"></script>
@endsection
