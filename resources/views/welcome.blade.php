@extends('layouts.master')

@section('title', 'RHA')

@section('content')
<style>
    .spb-asset-content p{
        color:#fff !important;
    }
    .title-wrap h3{
        color: #fff !important;
    }
</style>
@include('layouts.topmenu')
<div id="sf-mobile-slideout-backdrop"></div>
<div class="swift-slider-outer">
    <div id="swift-slider-1" class="swift-slider swiper-container" data-type="slider" data-fullscreen="0" data-max-height="800" data-transition="slide" data-loop="true" data-slide-count="2" data-autoplay="" data-continue="0">
        <div class="swiper-wrapper">

            <div class="swiper-slide image-slide dynamic-header-change" data-slide-id="1" data-slide-title="Product 1" style="background-image: url('images/slider2.jpg');background-color: " data-bg-size="cover" data-bg-align="top" data-bg-horiz-align="center" data-mobile-bg-horiz-align="center" data-slide-img="" data-style="dark" data-header-style="dark">
                <div class="caption-wrap container">
                    <div class="caption-content" data-caption-color="" data-caption-x="left" data-caption-y="middle" data-caption-size="smaller">
                        <h2 class="caption-title"><br>Remarkable Hospitality</h2>
                        <div class="caption-excerpt">
                            <!--<h3>Worldwide exclusive to uplift.</h3>-->
                            <a class="sf-button large white icon  sf-button-rounded dropshadow sf-button-has-icon" href="{{url('VisitRwanda')}}" target="_self"><span class="text">Visit Rwanda</span><i class="fa fa-eye"></i></a><a class="sf-button large white icon  sf-button-rounded dropshadow sf-button-has-icon" href="{{url('News')}}" target="_self"><span class="text">News</span><i class="fa fa-eye"></i></a>
                        </div>
                    </div>
                </div>
            </div>

            <div class="swiper-slide image-slide dynamic-header-change" data-slide-id="1" data-slide-title="Product 1" style="background-image: url('images/onomo3.jpg');background-color: " data-bg-size="cover" data-bg-align="top" data-bg-horiz-align="center" data-mobile-bg-horiz-align="center" data-slide-img="" data-style="dark" data-header-style="dark">
                <div class="caption-wrap container">
                    <div class="caption-content" data-caption-color="" data-caption-x="left" data-caption-y="middle" data-caption-size="smaller">
                        <h2 class="caption-title"><br>Remarkable Hospitality</h2>
                        <div class="caption-excerpt">
                            <!--<h3>Worldwide exclusive to uplift.</h3>-->
                            <a class="sf-button large white icon  sf-button-rounded dropshadow sf-button-has-icon" href="{{url('VisitRwanda')}}" target="_self"><span class="text">Visit Rwanda</span><i class="fa fa-eye"></i></a><a class="sf-button large white icon  sf-button-rounded dropshadow sf-button-has-icon" href="{{url('News')}}" target="_self"><span class="text">News</span><i class="fa fa-eye"></i></a>
                        </div>
                    </div>
                </div>
            </div>


            <div class="swiper-slide image-slide dynamic-header-change" data-slide-id="1" data-slide-title="Product 1" style="background-image: url('images/fivevolcano.jpg');background-color: " data-bg-size="cover" data-bg-align="top" data-bg-horiz-align="center" data-mobile-bg-horiz-align="center" data-slide-img="" data-style="dark" data-header-style="dark">
                <div class="caption-wrap container">
                    <div class="caption-content" data-caption-color="" data-caption-x="left" data-caption-y="middle" data-caption-size="smaller">
                        <h2 class="caption-title"><br>Remarkable Hospitality</h2>
                        <div class="caption-excerpt">
                            <!--<h3>Worldwide exclusive to uplift.</h3>-->
                            <a class="sf-button large white icon  sf-button-rounded dropshadow sf-button-has-icon" href="{{url('VisitRwanda')}}" target="_self"><span class="text">Visit Rwanda</span><i class="fa fa-eye"></i></a><a class="sf-button large white icon  sf-button-rounded dropshadow sf-button-has-icon" href="{{url('News')}}" target="_self"><span class="text">News</span><i class="fa fa-eye"></i></a></div>
                    </div>
                </div>
            </div>



            <div class="swiper-slide image-slide dynamic-header-change" data-slide-id="1" data-slide-title="Product 1" style="background-image: url('images/parkinn.jpg');background-color: " data-bg-size="cover" data-bg-align="top" data-bg-horiz-align="center" data-mobile-bg-horiz-align="center" data-slide-img="" data-style="dark" data-header-style="dark">
                <div class="caption-wrap container">
                    <div class="caption-content" data-caption-color="" data-caption-x="left" data-caption-y="middle" data-caption-size="smaller">
                        <h2 class="caption-title"><br>Remarkable Hospitality</h2>
                        <div class="caption-excerpt">
                            <!--<h3>Worldwide exclusive to uplift.</h3>-->
                            <a class="sf-button large white icon  sf-button-rounded dropshadow sf-button-has-icon" href="{{url('VisitRwanda')}}" target="_self"><span class="text">Visit Rwanda</span><i class="fa fa-eye"></i></a><a class="sf-button large white icon  sf-button-rounded dropshadow sf-button-has-icon" href="{{url('News')}}" target="_self"><span class="text">News</span><i class="fa fa-eye"></i></a></div>
                    </div>
                </div>
            </div>




            <div class="swiper-slide image-slide dynamic-header-change" data-slide-id="1" data-slide-title="Product 1" style="background-image: url('images/khana.jpg');background-color: " data-bg-size="cover" data-bg-align="top" data-bg-horiz-align="center" data-mobile-bg-horiz-align="center" data-slide-img="" data-style="dark" data-header-style="dark">
                <div class="caption-wrap container">
                    <div class="caption-content" data-caption-color="" data-caption-x="left" data-caption-y="middle" data-caption-size="smaller">
                        <h2 class="caption-title"><br>Remarkable Hospitality</h2>
                        <div class="caption-excerpt">
                            <!--<h3>Worldwide exclusive to uplift.</h3>-->
                            <a class="sf-button large white icon  sf-button-rounded dropshadow sf-button-has-icon" href="{{url('VisitRwanda')}}" target="_self"><span class="text">Visit Rwanda</span><i class="nucleo-icon-eye"></i></a><a class="sf-button large white icon  sf-button-rounded dropshadow sf-button-has-icon" href="{{url('News')}}" target="_self"><span class="text">News</span><i class="fa fa-eye"></i></a></div>
                    </div>
                </div>
            </div>

        </div>
        <a class="swift-slider-prev" href="index.html#">
            <svg version="1.1" class="svg-swift-slider-prev" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="48px" height="48px" viewBox="0 0 48 48" enable-background="new 0 0 48 48" xml:space="preserve">
                     <path fill="none" stroke="#222222" stroke-width="3" stroke-linecap="square" stroke-linejoin="round" stroke-miterlimit="10" d="
                        M14,24L34,4L14,24z" />
                <path fill="none" stroke="#222222" stroke-width="3" stroke-linecap="square" stroke-linejoin="round" stroke-miterlimit="10" d="
                        M14,24l20,20L14,24z" />
                  </svg>
            <h4>Previous</h4>
        </a>
        <a class="swift-slider-next" href="index.html#">
            <svg version="1.1" class="svg-swift-slider-next" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="48px" height="48px" viewBox="0 0 48 48" enable-background="new 0 0 48 48" xml:space="preserve">
                     <path fill="none" stroke="#222222" stroke-width="3" stroke-linecap="square" stroke-linejoin="round" stroke-miterlimit="10" d="
                        M34,24L14,44L34,24z" />
                <path fill="none" stroke="#222222" stroke-width="3" stroke-linecap="square" stroke-linejoin="round" stroke-miterlimit="10" d="
                        M34,24L14,4L34,24z" />
                  </svg>
            <h4>Next</h4>
        </a>
        <div class="swift-slider-pagination">
            <div class="dot"><span class=""></span></div>
            <div class="dot"><span class=""></span></div>
        </div>
        <div id="swift-slider-loader" class="circle">
            <div class="sf-svg-loader"><object data="../../wp-content/themes/uplift/images/loader-svgs/loader-32px-glyph_x-circle-08.svg" type="image/svg+xml"></object></div>
        </div>
    </div>
</div>
<div class="page_slider menu_transparent">
    <link href="css/Open+Sans.css" rel="stylesheet" property="stylesheet" type="text/css" media="all">
    <div id="rev_slider_1_1_wrapper" class="rev_slider_wrapper fullscreen-container" data-source="gallery" style="background:#000000;padding:0px;">
        <!-- START REVOLUTION SLIDER 5.4.5.1 fullscreen mode -->
        <div id="rev_slider_1_1" class="rev_slider fullscreenbanner" style="display:none;" data-version="5.4.5.1">
            <ul>
                <!-- SLIDE  -->
                <li data-index="rs-1" data-transition="zoomout" data-slotamount="7" data-hideafterloop="0" data-hideslideonmobile="off" data-easein="default" data-easeout="default" data-masterspeed="300" data-thumb="http://themegoodsthemes-pzbycso8wng.stackpathdns.com/altair/demo/wp-content/uploads/2014/10/1600x1200-1-100x50.jpg" data-rotate="0" data-saveperformance="off" data-title="Venezia, Italy" data-param1="" data-param2="" data-param3="" data-param4="" data-param5="" data-param6="" data-param7="" data-param8="" data-param9="" data-param10="" data-description="">
                    <!-- MAIN IMAGE -->
                    <img src="images/tours5.jpeg" alt="" title="1600&#215;1200-1" width="1600" height="1200" data-bgposition="center top" data-bgfit="cover" data-bgrepeat="no-repeat" class="rev-slidebg" data-no-retina>
                    <!-- LAYERS -->
                    <!-- LAYER NR. 1 -->
                    <div class="tp-caption pp_subheader   tp-resizeme" id="slide-1-layer-1" data-x="center" data-hoffset="" data-y="bottom" data-voffset="300" data-width="['auto']" data-height="['auto']" data-type="text" data-responsive_offset="on" data-frames='[{"delay":1000,"speed":600,"frame":"0","from":"x:-50px;opacity:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":600,"frame":"999","to":"x:-50px;opacity:0;","ease":"nothing"}]' data-textAlign="['inherit','inherit','inherit','inherit']" data-paddingtop="[0,0,0,0]" data-paddingright="[0,0,0,0]" data-paddingbottom="[0,0,0,0]" data-paddingleft="[0,0,0,0]" style="z-index: 5; white-space: nowrap; color: #ffffff; letter-spacing: px;font-family:Open Sans;font-style:italic;border-color:rgb(255,255,255);border-width:0px 0px 2px 0px 0px 0px 2px 0px 0px 0px 2px 0px 0px 0px 2px 0px;">Italy </div>
                    <!-- LAYER NR. 2 -->
                    <div class="tp-caption pp_header   tp-resizeme" id="slide-1-layer-2" data-x="center" data-hoffset="" data-y="bottom" data-voffset="180" data-width="['auto']" data-height="['auto']" data-type="text" data-responsive_offset="on" data-frames='[{"delay":1300,"speed":600,"frame":"0","from":"x:50px;opacity:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":600,"frame":"999","to":"x:50px;opacity:0;","ease":"nothing"}]' data-textAlign="['inherit','inherit','inherit','inherit']" data-paddingtop="[0,0,0,0]" data-paddingright="[0,0,0,0]" data-paddingbottom="[0,0,0,0]" data-paddingleft="[0,0,0,0]" style="z-index: 6; white-space: nowrap; letter-spacing: px;">Venezia </div>
                    <!-- LAYER NR. 3 -->
                    <div class="tp-caption pp_content   tp-resizeme" id="slide-1-layer-3" data-x="center" data-hoffset="" data-y="bottom" data-voffset="100" data-width="['auto']" data-height="['auto']" data-type="text" data-responsive_offset="on" data-frames='[{"delay":1600,"speed":600,"frame":"0","from":"y:50px;opacity:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":600,"frame":"999","to":"y:50px;opacity:0;","ease":"nothing"}]' data-textAlign="['inherit','inherit','inherit','inherit']" data-paddingtop="[0,0,0,0]" data-paddingright="[0,0,0,0]" data-paddingbottom="[0,0,0,0]" data-paddingleft="[0,0,0,0]" style="z-index: 7; white-space: nowrap; letter-spacing: px;">Arctic temperatures and several meters of snow
                        <br/> are no match for the natural impressions and cultural twist
                        <br/> that will warm your soul while dog sledding in Greenland.
                        <br/>
                    </div>
                </li>
                <!-- SLIDE  -->
                <li data-index="rs-2" data-transition="zoomout" data-slotamount="7" data-hideafterloop="0" data-hideslideonmobile="off" data-easein="default" data-easeout="default" data-masterspeed="300" data-thumb="http://themegoodsthemes-pzbycso8wng.stackpathdns.com/altair/demo/wp-content/uploads/2014/10/1600x1200-2-100x50.jpg" data-rotate="0" data-saveperformance="off" data-title="Zurich, Switzerland" data-param1="" data-param2="" data-param3="" data-param4="" data-param5="" data-param6="" data-param7="" data-param8="" data-param9="" data-param10="" data-description="">
                    <!-- MAIN IMAGE -->
                    <img src="images/slider2.JPG" alt="" title="1600&#215;1200-2" width="1600" height="1200" data-bgposition="center top" data-bgfit="cover" data-bgrepeat="no-repeat" class="rev-slidebg" data-no-retina>
                    <!-- LAYERS -->
                    <!-- LAYER NR. 4 -->
                    <div class="tp-caption pp_subheader   tp-resizeme" id="slide-2-layer-4" data-x="center" data-hoffset="0" data-y="bottom" data-voffset="300" data-width="auto" data-height="auto" data-type="text" data-responsive_offset="on" data-frames='[{"from":"x:-50px;opacity:0;","speed":600,"to":"","delay":1000,"ease":"Power3.easeInOut"},{"delay":"wait","speed":600,"to":"x:-50px;opacity:0;","ease":"nothing"}]' data-textAlign="['','','','']" style="z-index: 8; white-space: nowrap; color: #ffffff;text-transform:left;font-style:italic;border-color:rgb(255,255,255);">Switzerland </div>
                    <!-- LAYER NR. 5 -->
                    <div class="tp-caption pp_header   tp-resizeme" id="slide-2-layer-5" data-x="center" data-hoffset="0" data-y="bottom" data-voffset="180" data-width="auto" data-height="auto" data-type="text" data-responsive_offset="on" data-frames='[{"from":"x:50px;opacity:0;","speed":600,"to":"","delay":1300,"ease":"Power3.easeInOut"},{"delay":"wait","speed":600,"to":"x:50px;opacity:0;","ease":"nothing"}]' data-textAlign="['','','','']" style="z-index: 9; white-space: nowrap;text-transform:left;">Zurich </div>
                    <!-- LAYER NR. 6 -->
                    <div class="tp-caption pp_content   tp-resizeme" id="slide-2-layer-6" data-x="center" data-hoffset="0" data-y="bottom" data-voffset="100" data-width="auto" data-height="auto" data-type="text" data-responsive_offset="on" data-frames='[{"from":"y:50px;opacity:0;","speed":600,"to":"","delay":1600,"ease":"Power3.easeInOut"},{"delay":"wait","speed":600,"to":"y:50px;opacity:0;","ease":"nothing"}]' data-textAlign="['','','','']" style="z-index: 10; white-space: nowrap;text-transform:left;">Arctic temperatures and several meters of snow
                        <br/> are no match for the natural impressions and cultural twist
                        <br/> that will warm your soul while dog sledding in Greenland.
                        <br/>
                    </div>
                </li>
                <!-- SLIDE  -->
                <li data-index="rs-3" data-transition="zoomout" data-slotamount="7" data-hideafterloop="0" data-hideslideonmobile="off" data-easein="default" data-easeout="default" data-masterspeed="300" data-thumb="http://themegoodsthemes-pzbycso8wng.stackpathdns.com/altair/demo/wp-content/uploads/2014/10/1600x1200-4-100x50.jpg" data-rotate="0" data-saveperformance="off" data-title="Paris, France" data-param1="" data-param2="" data-param3="" data-param4="" data-param5="" data-param6="" data-param7="" data-param8="" data-param9="" data-param10="" data-description="">
                    <!-- MAIN IMAGE -->
                    <img src="images/slider1.JPG" alt="" title="1600&#215;1200-4" width="1600" height="1200" data-bgposition="center top" data-bgfit="cover" data-bgrepeat="no-repeat" class="rev-slidebg" data-no-retina>
                    <!-- LAYERS -->
                    <!-- LAYER NR. 7 -->
                    <div class="tp-caption pp_subheader   tp-resizeme" id="slide-3-layer-7" data-x="center" data-hoffset="0" data-y="bottom" data-voffset="300" data-width="auto" data-height="auto" data-type="text" data-responsive_offset="on" data-frames='[{"from":"x:-50px;opacity:0;","speed":600,"to":"","delay":1000,"ease":"Power3.easeInOut"},{"delay":"wait","speed":600,"to":"x:-50px;opacity:0;","ease":"nothing"}]' data-textAlign="['','','','']" style="z-index: 11; white-space: nowrap; color: #ffffff;text-transform:left;font-style:italic;border-color:rgb(255,255,255);">France </div>
                    <!-- LAYER NR. 8 -->
                    <div class="tp-caption pp_header   tp-resizeme" id="slide-3-layer-8" data-x="center" data-hoffset="0" data-y="bottom" data-voffset="180" data-width="auto" data-height="auto" data-type="text" data-responsive_offset="on" data-frames='[{"from":"x:50px;opacity:0;","speed":600,"to":"","delay":1300,"ease":"Power3.easeInOut"},{"delay":"wait","speed":600,"to":"x:50px;opacity:0;","ease":"nothing"}]' data-textAlign="['','','','']" style="z-index: 12; white-space: nowrap;text-transform:left;">Paris </div>
                    <!-- LAYER NR. 9 -->
                    <div class="tp-caption pp_content   tp-resizeme" id="slide-3-layer-9" data-x="center" data-hoffset="0" data-y="bottom" data-voffset="100" data-width="auto" data-height="auto" data-type="text" data-responsive_offset="on" data-frames='[{"from":"y:50px;opacity:0;","speed":600,"to":"","delay":1600,"ease":"Power3.easeInOut"},{"delay":"wait","speed":600,"to":"y:50px;opacity:0;","ease":"nothing"}]' data-textAlign="['','','','']" style="z-index: 13; white-space: nowrap;text-transform:left;">Arctic temperatures and several meters of snow
                        <br/> are no match for the natural impressions and cultural twist
                        <br/> that will warm your soul while dog sledding in Greenland.
                        <br/>
                    </div>
                </li>
            </ul>
            <script>
                var htmlDiv = document.getElementById("rs-plugin-settings-inline-css");
                var htmlDivCss = "";
                if (htmlDiv) {
                    htmlDiv.innerHTML = htmlDiv.innerHTML + htmlDivCss;
                } else {
                    var htmlDiv = document.createElement("div");
                    htmlDiv.innerHTML = "<style>" + htmlDivCss + "</style>";
                    document.getElementsByTagName("head")[0].appendChild(htmlDiv.childNodes[0]);
                }
            </script>
            <div class="tp-bannertimer tp-bottom" style="visibility: hidden !important;"></div>
        </div>
        <script>
            var htmlDiv = document.getElementById("rs-plugin-settings-inline-css");
            var htmlDivCss = ".tp-caption.pp_subheader,.pp_subheader{font-size:16px;line-height:30px;font-weight:600;color:rgb(255,255,255);font-style:italic;text-decoration:none;background-color:transparent;border-width:0px 0px 2px 0px;border-color:rgb(255,255,255);border-style:solid;text-transform:uppercase;text-shadow:none}.tp-caption.pp_header,.pp_header{color:#ffffff;background-color:transparent;text-decoration:none;font-size:80px;line-height:114px;font-weight:700;font-family:Raleway;border-width:0px;border-color:rgb(0,0,0);border-style:none;text-shadow:none;text-transform:uppercase}.tp-caption.pp_content,.pp_content{color:#ffffff;background-color:transparent;text-decoration:none;font-size:16px;line-height:26px;font-weight:500;font-family:Raleway;border-width:0px;border-color:rgb(0,0,0);border-style:none;text-shadow:none}";
            if (htmlDiv) {
                htmlDiv.innerHTML = htmlDiv.innerHTML + htmlDivCss;
            } else {
                var htmlDiv = document.createElement("div");
                htmlDiv.innerHTML = "<style>" + htmlDivCss + "</style>";
                document.getElementsByTagName("head")[0].appendChild(htmlDiv.childNodes[0]);
            }
        </script>
        <script type="text/javascript">
            setREVStartSize({
                c: jQuery('#rev_slider_1_1'),
                gridwidth: [960],
                gridheight: [868],
                sliderLayout: 'fullscreen',
                fullScreenAutoWidth: 'off',
                fullScreenAlignForce: 'off',
                fullScreenOffsetContainer: '',
                fullScreenOffset: ''
            });

            var revapi1,
                tpj = jQuery;
            tpj.noConflict();
            tpj(document).ready(function() {
                if (tpj("#rev_slider_1_1").revolution == undefined) {
                    revslider_showDoubleJqueryError("#rev_slider_1_1");
                } else {
                    revapi1 = tpj("#rev_slider_1_1").show().revolution({
                        sliderType: "standard",
                        jsFileLocation: "//themes.themegoods.com/altair/demo/wp-content/plugins/revslider/public/assets/js/",
                        sliderLayout: "fullscreen",
                        dottedOverlay: "none",
                        delay: 9000,
                        navigation: {
                            keyboardNavigation: "off",
                            keyboard_direction: "horizontal",
                            mouseScrollNavigation: "off",
                            mouseScrollReverse: "default",
                            onHoverStop: "on",
                            touch: {
                                touchenabled: "on",
                                touchOnDesktop: "off",
                                swipe_threshold: 75,
                                swipe_min_touches: 1,
                                swipe_direction: "horizontal",
                                drag_block_vertical: false
                            },
                            arrows: {
                                style: "gyges",
                                enable: true,
                                hide_onmobile: false,
                                hide_onleave: false,
                                tmp: '',
                                left: {
                                    h_align: "left",
                                    v_align: "center",
                                    h_offset: 20,
                                    v_offset: 0
                                },
                                right: {
                                    h_align: "right",
                                    v_align: "center",
                                    h_offset: 20,
                                    v_offset: 0
                                }
                            },
                            bullets: {
                                enable: true,
                                hide_onmobile: false,
                                style: "uranus",
                                hide_onleave: false,
                                direction: "horizontal",
                                h_align: "center",
                                v_align: "bottom",
                                h_offset: 0,
                                v_offset: 20,
                                space: 5,
                                tmp: '<span class="tp-bullet-inner"></span>'
                            }
                        },
                        visibilityLevels: [1240, 1024, 778, 480],
                        gridwidth: 960,
                        gridheight: 868,
                        lazyType: "none",
                        shadow: 0,
                        spinner: "spinner2",
                        stopLoop: "off",
                        stopAfterLoops: -1,
                        stopAtSlide: -1,
                        shuffle: "off",
                        autoHeight: "off",
                        fullScreenAutoWidth: "off",
                        fullScreenAlignForce: "off",
                        fullScreenOffsetContainer: "",
                        fullScreenOffset: "",
                        disableProgressBar: "on",
                        hideThumbsOnMobile: "off",
                        hideSliderAtLimit: 0,
                        hideCaptionAtLimit: 0,
                        hideAllCaptionAtLilmit: 0,
                        debugMode: false,
                        fallbacks: {
                            simplifyAll: "off",
                            nextSlideOnWindowFocus: "off",
                            disableFocusListener: false,
                        }
                    });
                }

            }); /*ready*/
        </script>
        <script>
            var htmlDivCss = ' #rev_slider_1_1_wrapper .tp-loader.spinner2{ background-color: #ffffff !important; } ';
            var htmlDiv = document.getElementById('rs-plugin-settings-inline-css');
            if (htmlDiv) {
                htmlDiv.innerHTML = htmlDiv.innerHTML + htmlDivCss;
            } else {
                var htmlDiv = document.createElement('div');
                htmlDiv.innerHTML = '<style>' + htmlDivCss + '</style>';
                document.getElementsByTagName('head')[0].appendChild(htmlDiv.childNodes[0]);
            }
        </script>
        <script>
            var htmlDivCss = unescape("%0A%23rev_slider_1_1%20.uranus%20.tp-bullet%7B%0A%20%20border-radius%3A%2050%25%3B%0A%20%20box-shadow%3A%200%200%200%202px%20rgba%28255%2C%20255%2C%20255%2C%200%29%3B%0A%20%20-webkit-transition%3A%20box-shadow%200.3s%20ease%3B%0A%20%20transition%3A%20box-shadow%200.3s%20ease%3B%0A%20%20background%3Atransparent%3B%0A%20%20width%3A15px%3B%0A%20%20height%3A15px%3B%0A%7D%0A%23rev_slider_1_1%20.uranus%20.tp-bullet.selected%2C%0A%23rev_slider_1_1%20.uranus%20.tp-bullet%3Ahover%20%7B%0A%20%20box-shadow%3A%200%200%200%202px%20rgba%28255%2C%20255%2C%20255%2C1%29%3B%0A%20%20border%3Anone%3B%0A%20%20border-radius%3A%2050%25%3B%0A%20%20background%3Atransparent%3B%0A%7D%0A%0A%23rev_slider_1_1%20.uranus%20.tp-bullet-inner%20%7B%0A%20%20-webkit-transition%3A%20background-color%200.3s%20ease%2C%20-webkit-transform%200.3s%20ease%3B%0A%20%20transition%3A%20background-color%200.3s%20ease%2C%20transform%200.3s%20ease%3B%0A%20%20top%3A%200%3B%0A%20%20left%3A%200%3B%0A%20%20width%3A%20100%25%3B%0A%20%20height%3A%20100%25%3B%0A%20%20outline%3A%20none%3B%0A%20%20border-radius%3A%2050%25%3B%0A%20%20background-color%3A%20rgb%28255%2C%20255%2C%20255%29%3B%0A%20%20background-color%3A%20rgba%28255%2C%20255%2C%20255%2C%200.3%29%3B%0A%20%20text-indent%3A%20-999em%3B%0A%20%20cursor%3A%20pointer%3B%0A%20%20position%3A%20absolute%3B%0A%7D%0A%0A%23rev_slider_1_1%20.uranus%20.tp-bullet.selected%20.tp-bullet-inner%2C%0A%23rev_slider_1_1%20.uranus%20.tp-bullet%3Ahover%20.tp-bullet-inner%7B%0A%20transform%3A%20scale%280.4%29%3B%0A%20-webkit-transform%3A%20scale%280.4%29%3B%0A%20background-color%3Argb%28255%2C%20255%2C%20255%29%3B%0A%7D%0A");
            var htmlDiv = document.getElementById('rs-plugin-settings-inline-css');
            if (htmlDiv) {
                htmlDiv.innerHTML = htmlDiv.innerHTML + htmlDivCss;
            } else {
                var htmlDiv = document.createElement('div');
                htmlDiv.innerHTML = '<style>' + htmlDivCss + '</style>';
                document.getElementsByTagName('head')[0].appendChild(htmlDiv.childNodes[0]);
            }
        </script>
    </div>
    <!-- END REVOLUTION SLIDER -->
</div>
<div id="main-container" class="clearfix">

    <div class="inner-container-wrap">
        <div class="inner-page-wrap has-no-sidebar no-bottom-spacing no-top-spacing clearfix">
            <div class="clearfix">
                <div class="page-content hfeed clearfix">
                    <div class="clearfix post-9 page type-page status-publish hentry" id="9">
                        <section data-header-style="" class="row fw-row  dynamic-header-change">
                            <div class="spb-row-container spb-row-content-width spb_parallax_asset sf-parallax parallax-content-height parallax-scroll spb_content_element bg-type-cover col-sm-12  col-natural">
                                <div class="spb_content_element" style="background-image: url(images/rad.jpg);background-position: center;background-size: cover;">
                                    <section class="container ">
                                        <div class="row">
                                            <div class="blank_spacer col-sm-12" style="height:60px;"></div>
                                        </div>
                                    </section>
                                    <section class="container ">
                                        <div class="row">
                                            <div class="spb_content_element col-sm-6 spb_text_column">
                                                <div class="spb-asset-content" style="padding-top:0%;padding-bottom:0%;padding-left:0%;padding-right:0%;">
                                                    <div class="title-wrap">
                                                        <h3 class="spb-heading spb-text-heading"><span>Follow us on Facebook @Rwanda Hospitality Association</span></h3>
                                                    </div>
                                                    <iframe src="https://www.facebook.com/plugins/page.php?href=https%3A%2F%2Fweb.facebook.com%2FRwanda-Hospitality-Association-2054637401490796%2F&tabs=timeline&width=700&height=700&small_header=true&adapt_container_width=true&hide_cover=false&show_facepile=true&appId=1627823807509729" width="100%" height="400" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowTransparency="true" allow="encrypted-media"></iframe>
                                                </div>
                                            </div>
                                            <div class="spb_content_element col-sm-6 spb_text_column">
                                                <div class="spb-asset-content" style="padding-top:0%;padding-bottom:0%;padding-left:0%;padding-right:0%;">
                                                    <div class="title-wrap">
                                                        <h3 class="spb-heading spb-text-heading" ><span>Follow us on Twitter @RwandaHospitality</span></h3>

                                                    </div>

                                                    <a class="twitter-timeline" href="https://twitter.com/RwHospitality1?ref_src=twsrc%5Etfw" data-height="400">Tweets by RwandaHospitality</a> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
                                                </div>
                                            </div>
                                        </div>
                                    </section>
                                    <section class="container ">
                                        <div class="row">
                                            <div class="blank_spacer col-sm-12  " style="height:40px;"></div>
                                        </div>
                                    </section>
                                </div>
                                <div class="row-overlay" style="background-color:#222222;opacity:0.9;"></div>
                            </div>
                        </section>
                        <section data-header-style="" class="row fw-row  dynamic-header-change">
                            <div class="spb-row-container spb-row-content-width spb_parallax_asset sf-parallax parallax-content-height parallax-scroll spb_content_element bg-type-cover col-sm-12  col-natural">
                                <div class="spb_content_element" style="background: #fff">
                                    <section class="container ">
                                        <div class="row">
                                            <div class="blank_spacer col-sm-12" style="height:60px;"></div>
                                        </div>
                                    </section>
                                    <section class="row fw-row ">
                                        <div class="spb_portfolio_showcase_widget spb_content_element has-pagination col-sm-12">
                                            <a class="view-all hidden" href="portfolio/portfolio-3-column-standard/index.html"><i class="sf-icon-quickview"></i></a>
                                            <div class="spb-asset-content">
                                                <div class="title-wrap container">
                                                    <h3 class="spb-heading center-title" style="color: #000 !important;"><span>We are a member of</span></h3>
                                                </div>
                                                <div class="port-carousel carousel-wrap">
                                                    <a href="index.html#" class="carousel-prev"><i class="sf-icon-left-chevron"></i></a><a href="index.html#" class="carousel-next"><i class="sf-icon-right-chevron"></i></a>
                                                    <div id="carousel-1" class="portfolio-showcase carousel-items staged-carousel gutters clearfix" data-columns="5" data-auto="false" data-pagination="yes">
                                                        <div itemscope class="clearfix carousel-item portfolio-item gallery-item">
                                                            <figure class="animated-overlay overlay-style">
                                                                <a href="" class="link-to-post"></a>
                                                                <div class="img-wrap"><img itemprop="image" src="images/secmember3.png" width="500" height="375" alt="" style="display: none !important;"/></div>
                                                                <div class="figcaption-wrap"></div>
                                                            </figure>
                                                        </div>
                                                        <div itemscope class="clearfix carousel-item portfolio-item gallery-item">
                                                            <figure class="animated-overlay overlay-style">
                                                                <a href="" class="link-to-url"></a>
                                                                <div class="img-wrap"><img itemprop="image" src="images/secmember3.png" width="500" height="375" alt="" /></div>
                                                                <div class="figcaption-wrap"></div>
                                                            </figure>
                                                        </div>

                                                        <div itemscope class="clearfix carousel-item portfolio-item gallery-item" style="display: none !important;">
                                                            <figure class="animated-overlay overlay-style">
                                                                <a href="" class="link-to-url"></a>
                                                                <div class="img-wrap"><img itemprop="image" src="images/feder.png" width="500" height="375" alt="" /></div>
                                                                <div class="figcaption-wrap"></div>

                                                            </figure>
                                                        </div>
                                                        <div itemscope class="clearfix carousel-item portfolio-item gallery-item">
                                                            <figure class="animated-overlay overlay-style">
                                                                <a href="" class="link-to-post"></a>
                                                                <div class="img-wrap"><img itemprop="image" src="images/secmember2.png" width="500" height="375" alt="" /></div>
                                                                <div class="figcaption-wrap"></div>

                                                            </figure>
                                                        </div>

                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </section>
                                    <section class="container ">
                                        <div class="row">
                                            <div class="blank_spacer col-sm-12  " style="height:40px;"></div>
                                        </div>
                                    </section>
                                </div>
                                <div class="row-overlay" style="background-color:#222222;opacity:0.9;"></div>
                            </div>
                        </section>
                        <section data-header-style="" class="row fw-row  dynamic-header-change" >
                            <div class="inner-container-wrap" style="background: linear-gradient(rgb(106, 68, 43), rgba(0, 0, 0, 0.1)),url(images/footer.jpg);background-size: contain;">
                                <div class="inner-page-wrap has-no-sidebar no-top-spacing clearfix">

                                    <div class="clearfix">
                                        <div class="page-content hfeed clearfix">
                                            <div class="clearfix post-13072 page type-page status-publish hentry" id="13072">

                                                <section class="container "><div class="row"><div class="blank_spacer col-sm-12  " style="height:60px;"></div>
                                                    </div></section>
                                                <section class="container "><div class="row">
                                                        <div class="spb_content_element col-sm-5 spb_text_column">
                                                            <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d15949.999335745973!2d30.1035248!3d-1.9533694!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xc76b5e00aaa31b51!2sM%26M+Plaza!5e0!3m2!1sen!2srw!4v1530612631578" width="100%" height="300" frameborder="0" style="border:0" allowfullscreen></iframe>
                                                        </div>
                                                        <div class="spb-column-container col-sm-4   " style="padding-left:15px; padding-right:15px; ">
                                                            <div class="spb-asset-content" style="">
                                                                <section class="container "><div class="row">
                                                                        <div class="spb_content_element col-sm-12 spb_text_column">
                                                                            <div class="spb-asset-content" style="padding-top:0%;padding-bottom:0%;padding-left:0%;padding-right:0%;">
                                                                                <div class="title-wrap"><h3 class="spb-heading spb-text-heading"><span>Office Contacts</span></h3></div>
                                                                                <p>Call: +250 788304524<br />
                                                                                    <!--Email: <a href="" class="__cf_email__">info@rha.rw</a></p>-->
                                                                                    Email: <a href="" class="__cf_email__">info@rha.rw</a> or <br>
                                                                                    <a href="" class="__cf_email__">rwandahospitalityassociation@gmail.com</a></p>
                                                                            </div>
                                                                        </div> </div></section>
                                                                <section class="container "><div class="row"><div class="blank_spacer col-sm-12  " style="height:30px;"></div>
                                                                    </div></section>
                                                                <section class="container "><div class="row">
                                                                        <div class="spb_content_element col-sm-12 spb_text_column">
                                                                            <div class="spb-asset-content" style="padding-top:0%;padding-bottom:0%;padding-left:0%;padding-right:0%;">
                                                                                <div class="title-wrap"><h3 class="spb-heading spb-text-heading"><span>Follow us</span></h3></div>
                                                                                <ul class="social-icons standard ">
                                                                                    <li class="twitter"><a href="https://twitter.com/RwHospitality1" target="_blank"><i class="fa-twitter"></i><i class="fa-twitter"></i></a></li>
                                                                                    <li class="facebook"><a href="https://web.facebook.com/Rwanda-Hospitality-Association-2054637401490796/" target="_blank"><i class="fa-facebook"></i><i class="fa-facebook"></i></a></li>
                                                                                </ul>
                                                                            </div>
                                                                        </div> </div></section>
                                                            </div>
                                                        </div>
                                                        <div class="spb-column-container col-sm-3   " style="padding-left:15px; padding-right:15px; ">
                                                            <div class="spb-asset-content" style="">
                                                                <section class="container "><div class="row">
                                                                        <div class="spb_content_element col-sm-12 spb_text_column">
                                                                            <div class="spb-asset-content" style="padding-top:0%;padding-bottom:0%;padding-left:0%;padding-right:0%;">
                                                                                <div class="title-wrap"><h3 class="spb-heading spb-text-heading"><span>Our Address</span></h3></div>
                                                                                <p>Kigali,Rwanda<br />
                                                                                    Gishushu, KG 8 Avenue N.6<br />
                                                                                    M&M Plaza <br />
                                                                                    5th Floor</p>
                                                                            </div>
                                                                        </div> </div></section>
                                                            </div>
                                                        </div> </div></section>
                                                <div class="link-pages"></div>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </section>
                        <div class="link-pages"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="sf-full-header-search-backdrop"></div>
</div>
@include('layouts.footer')
@endsection
