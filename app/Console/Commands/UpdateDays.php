<?php

namespace App\Console\Commands;

use App\JoinMember;
use Carbon\Carbon;
use Illuminate\Console\Command;
use DateTime;

class UpdateDays extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'Member:UpdateDays';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Cron job to update days left';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $member = JoinMember::all();
        foreach ($member as $datas){

            $current_date = Carbon::now();
            $expiration_date = $datas->expiration_date;

            $datetime1 = new DateTime($current_date);
            $datetime2 = new DateTime($expiration_date);

            $interval = $datetime1->diff($datetime2);
            $days = $interval->format('%a'); //now do whatever you like with $days

            if($days == "0"){
                $updatedays = JoinMember::where('id',$datas->id)->update(['days_left'=>$days]);
                $updatestatus = JoinMember::where('id',$datas->id)->update(['Memberstatus'=>"Expired"]);
            }else{
                $updatedays = JoinMember::where('id',$datas->id)->update(['days_left'=>$days]);
            }

        }
        $this->info('Successfully updated days left');
    }
}
